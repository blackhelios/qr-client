
import React, { Component } from 'react'
import { Route, Switch } from 'react-router-dom'


import Home from './components/landing/Home'
import Login from './components/login-signup/admin/Login/Login'
import Donors from './components/landing/Donors/Donars'
import Contributors from './components/landing/Contributor/Contributor'
import Books from './components/landing/Books/Books'
import Authenticated from './components/misc/auth/Authenticated'
import { connect } from 'react-redux'

import AdminRoutes from './components/private/AdminRoutes'
import ModalManager from './components/misc/modalManager/modalManager';

import { getAllBooks } from './actions/bookActions'




class Routes extends Component {


  componentDidMount = () => {
    this.props.getAllBooks()
  }


  render() {
    return (




      <div>

        <ModalManager />

        <Switch>
          <Route exact component={Authenticated(Home, true)} path="/" />
          <Route exact component={Authenticated(Donors, true)} path="/donors" />
          <Route exact component={Contributors} path="/contributor" />
          <Route exact component={Books} path="/books" />
          <Route exact component={Login} path="/login-admin" />
          <AdminRoutes


          />

        </Switch>

      </div >




    )
  }


}




const mapDispatchToProps = {
  getAllBooks
}


export default connect(null, mapDispatchToProps)(Routes);

