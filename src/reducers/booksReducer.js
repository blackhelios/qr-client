import { ADD_BOOK, GET_BOOKS, EDIT_BOOK, GET_BOOK, DELETE_BOOK } from '../actions/types'

const initialState = {

}



export default (state = initialState, action) => {
    switch (action.type) {

        case ADD_BOOK:
            return { ...state, newbook: action.payload }
            break;
        case GET_BOOKS:
            return { ...state, books: action.payload }
            break;
        case EDIT_BOOK:
            return { ...state, book: action.payload }
            break;
        case GET_BOOK:
            return { ...state, bookToEdit: action.payload }
            break;
        case DELETE_BOOK:
            return { ...state }
            break;
        default:
            return state
    }
}
