import { ADD_ARTICLE, GET_ARTICLE, EDIT_ARTICLE, DELETE_ARTICLE, SEARCH_ARTICLE, CLEAR_IMAGES, UPLOAD_ARTICLES, DELETE_IMAGE, UPLOAD_IMAGES } from '../actions/types'


const initialState = {
    searchedArticle: null,
    articleToEdit: null,
    uploadedImages: []

}

export default (state = initialState, action) => {
    switch (action.type) {

        case ADD_ARTICLE:
            return { ...state }

            break;
        case SEARCH_ARTICLE:
            return { ...state, searchedArticle: action.payload }

            break;
        case GET_ARTICLE:
            return { ...state, articleToEdit: action.payload }

            break;
        case EDIT_ARTICLE:
            return { ...state, editedArticle: action.payload }

            break;
        case DELETE_IMAGE:

            let updatedImages = state.uploadedImages.filter((image) => {
                return image.public_id !== action.payload;
            })
            return { ...state, uploadedImages: updatedImages }

            break;
        case UPLOAD_IMAGES:
            return { ...state, uploadedImages: [...state.uploadedImages, action.payload] }

            break;
        case CLEAR_IMAGES:
            return { ...state, uploadedImages: [] }


        default:
            return state
    }
}
