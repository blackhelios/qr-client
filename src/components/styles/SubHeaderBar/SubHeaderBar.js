import React from 'react'
import { } from 'react-router-dom'
import {
    Navbar, Nav,
    NavItem,
    NavLink,
    NavbarBrand,
    Collapse

} from 'reactstrap'



function SubHeaderBar({ title, linkTo, color, link1, link2, buttontTxt }) {


    return (
        <div
            className="container-fluid my-3"
        >

            <div className="row">

                <div className="col-12">

                    <Navbar
                        color={color} light

                    >
                        <NavbarBrand
                            style={{
                                color: '#fff',
                                letterSpacing: '2px'

                            }}

                            href={linkTo}>{title}</NavbarBrand>


                        <Nav
                            className="ml-auto"
                            navbar

                        >
                            <NavItem

                            >
                                <NavLink
                                    href={link1}
                                    style={{
                                        color: '#fff'
                                    }}

                                >{buttontTxt}</NavLink>
                            </NavItem>


                        </Nav>




                    </Navbar>
                </div>

            </div>



        </div>
    )
}



export default SubHeaderBar;