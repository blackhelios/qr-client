import React, { Component, Fragment } from 'react'
import { items, adminItems } from './navitems'
import styles from './Navbar.module.css';
import { connect } from 'react-redux'
import { setLocale } from '../../actions/localeActions'
import { logoutUser } from '../../actions/userActions'




import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem
} from 'reactstrap';


class NavBar extends Component {


    style = {
        color: 'white',
        fontSize: '20px',
        letterSpacing: '2px',
        fontFamily: 'inherit'
    }

    state = {
        isOpen: false
    };

    toggleNavbar = () => {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }





    render() {



        const renderLinks = (links) => {
            return links.map((item, i) => {


                return (
                    <NavItem
                        key={i}
                    >
                        <NavLink
                            style={{
                                color: '#fff',
                                letterSpacing: '1px'
                            }}
                            href={item.linkTo}>

                            {item.name}

                        </NavLink>
                    </NavItem>
                )
            })


        }

        const renderNav = () => {


            if (this.props.user) {
                if (this.props.user.isAuth) {
                    return renderLinks(adminItems)

                } else {
                    return renderLinks(items)
                }
            }



        }



        const renderLogout = () => {
            if (this.props.user) {
                if (this.props.user.isAuth) {
                    return (<NavItem>
                        <NavLink onClick={() => { this.props.logoutUser() }}>Logout</NavLink>
                    </NavItem>)
                }

            }
        }


        const renderDropdown = () => {
            return (
                <UncontrolledDropdown nav inNavbar>
                    <DropdownToggle nav caret>
                        <i className="ni ni-world-2 text-white"></i>
                    </DropdownToggle>
                    <DropdownMenu right>
                        <DropdownItem
                            onClick={() => { this.props.setLocale('myanmar') }}

                        >
                            Myanmar
                     </DropdownItem>
                        <DropdownItem
                            onClick={() => { this.props.setLocale('english') }}

                        >
                            English
                       </DropdownItem>

                    </DropdownMenu>
                </UncontrolledDropdown>
            )
        }



        const renderNavItems = () => {
            return (

                <Collapse isOpen={this.state.isOpen} navbar>
                    <Nav className="ml-auto" navbar >
                        <i className={` ${styles.toggleBtn} ni ni-fat-remove ml-auto`} onClick={this.toggleNavbar}></i>
                        {renderNav()}
                        {renderLogout()}
                        {renderDropdown()}


                    </Nav>
                </Collapse>

            )


        }







        return (
            <Fragment>
                <div>
                    <Navbar
                        expand="md"
                        color="green"

                    >
                        <NavbarBrand style={this.style} href="/">Quren Reader</NavbarBrand>
                        {/* <NavbarToggler onClick={this.toggleNavbar} style={{ backgroundColor: 'white' }} /> */}
                        <button className="navbar-toggler btn" type="button" onClick={this.toggleNavbar}>
                            <i className="ni ni-align-left-2 text-white"></i>
                        </button>
                        {renderNavItems()}
                    </Navbar>
                </div>

            </Fragment >
        )
    }
}


// const mapStateToProps = (state) => ({
//     user: state.user.userData
// })

const mapDispatchToProps = {
    setLocale,
    logoutUser

}



export default connect(null, mapDispatchToProps)(NavBar);