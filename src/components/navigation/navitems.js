export const items = [

    {
        name: 'Books',
        linkTo: '/books'

    },
    {
        name: 'Donors',
        linkTo: '/donors'

    },
    {
        name: 'About Contributor',
        linkTo: '/contributor'

    },
    {
        name: 'Login',
        linkTo: '/login-admin'

    }






]


export const adminItems = [
    {
        name: 'Books',
        linkTo: '/books'

    },
    {
        name: 'Donors',
        linkTo: '/donors'

    },
    {
        name: 'About Contributor',
        linkTo: '/contributor'

    },
    {
        name: 'User DashBoard',
        linkTo: '/admin'

    },


]