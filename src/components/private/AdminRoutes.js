import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { Switch, Route } from 'react-router-dom'
import DashBoard from './dashboard/DashBoard'
import Authenticated from '../misc/auth/Authenticated'
import HeaderFooter from '../landing/Layout/HeaderFooter'
import Donors from './donors/Donors'
import Articles from './articles/Articles'
import Users from './users/Users'
import Settings from './settings/Settings'
import Books from './books/Books'
import { ListGroup, ListGroupItem } from 'reactstrap';











class AdminRoutes extends Component {








  render() {


    const { user } = this.props;




    const headerTitle = (
      <div className="col-12 text-center">
        <h4
          className="display-4"
          style={{ letterSpacing: '1px' }}
        >

          Welcome {user && user.username}

        </h4>

      </div>
    )



    const renderSidebarLists = () => {

      return (
        <ListGroup>
          <ListGroupItem tag="a" href="/admin" action>Home</ListGroupItem>
          <ListGroupItem tag="a" href="/admin/books" action>Books</ListGroupItem>
          <ListGroupItem tag="a" href="/admin/articles" action>Articles</ListGroupItem>
          <ListGroupItem tag="a" href="/admin/donors" action>Donors</ListGroupItem>
          <ListGroupItem tag="a" href="/admin/users" action>Users</ListGroupItem>
          <ListGroupItem tag="a" href="/admin/settings" action>Settings</ListGroupItem>
        </ListGroup>
      )

    }


    const renderDashboard = () => {






      return (
        <Fragment>
          <HeaderFooter
          >


            <section
              className="my-4"
            >

              <div className="container-fluid">

                <div className="row">
                  {headerTitle}
                </div>


                <div className="row">

                  <div className="col-lg-2">
                    {renderSidebarLists()}
                  </div>

                  <div className="col-lg-10">

                    <Switch>
                      <Route path="/admin" exact component={Authenticated(DashBoard)} />
                      <Route path="/admin/articles" component={Articles} />
                      <Route path="/admin/books" component={Books} />
                      <Route path="/admin/donors" component={Donors} />
                      <Route path="/admin/users" component={Users} />
                      <Route path="/admin/settings" component={Settings} />
                    </Switch>

                  </div>

                </div>






              </div>



            </section>




          </HeaderFooter>

        </Fragment>
      )






    }


    return (


      <Fragment>
        {renderDashboard()}
      </Fragment>
    )





  }
}







const mapStateToProps = (state) => ({
  user: state.user.userData,


})




export default connect(mapStateToProps)(AdminRoutes);