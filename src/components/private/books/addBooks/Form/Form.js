import React from 'react'
import TextInput from '../../../../misc/forms/inputs/TextInput'
import { reduxForm, Field } from 'redux-form'


function Form({ handleSubmit, valid,
    submitCB,
    submitting,
    pristine,
    errors,
    initialValues

}) {






    return (
        <form
            onSubmit={handleSubmit(submitCB)}


        >
            <h4 className="text-center">

                {
                    initialValues ? 'Edit Book' : 'Add Book '
                }
            </h4>
            <Field
                component={TextInput}
                placeholder="book name"
                name="name"
            />

            <Field
                name="author"
                component={TextInput}
                placeholder="author"
            />

            <button
                className="btn btn-success btn-block"
                type="submit"
            >

                {
                    initialValues ? 'Edit Book' : 'Add Book '
                }

            </button>


        </form>
    )
}




export default reduxForm({
    form: 'addbook'



})(Form);
