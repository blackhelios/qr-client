import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import AddBookForm from './Form/Form'
import { isEmpty } from '../../../../utils/isEmpty'
import { addBook, editBook } from '../../../../actions/bookActions'



class AddTable extends Component {




    handleBookSubmit = (data) => {



        if (!isEmpty(this.props.initialValues)) {
            console.log('edited');
            this.props.editBook(this.props.initialValues._id, data)

        } else {
            console.log('created');
            this.props.addBook(data)
        }



    }





    render() {







        return (
            <Fragment>

                <div
                    className="container"
                >


                    <AddBookForm
                        submitCB={this.handleBookSubmit}
                        initialValues={this.props.initialValues}

                    />


                </div>



            </Fragment>
        )
    }
}

const mapStateToProps = (state) => ({
    initialValues: state.books.bookToEdit
})

const mapDispatchToProps = {
    addBook,
    editBook
}


export default connect(mapStateToProps, mapDispatchToProps)(AddTable);