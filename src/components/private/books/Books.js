import React, { Component, Fragment } from 'react'
import BookTable from './booksTable/BookTable'
import { Switch, Route } from 'react-router-dom'
import SubHeaderBar from '../../styles/SubHeaderBar/SubHeaderBar'
import AddBook from './addBooks/AddBooks'
import Authenticated from '../../misc/auth/Authenticated'

import { connect } from 'react-redux'





class Books extends Component {





    render() {

        const { books } = this.props;
        console.log(books.books);
        return (
            <Fragment>

                <SubHeaderBar
                    title="Manage Books"
                    linkTo="/admin/books"
                    color="green"
                    link1="/admin/books/add"
                    link2="/admin/books"
                    buttontTxt="add books"

                />


                <div className="container">
                    <h4 className="text-center">
                        Total : {books.books && books.books.length} Books
                        </h4>
                    <div className="row p-2">

                        <div className="col-12">
                            {books &&
                                <Switch>
                                    <Route exact path="/admin/books" component={() => <BookTable books={books} />}

                                    />
                                    <Route path="/admin/books/add" component={AddBook} />

                                </Switch>


                            }




                        </div>


                    </div>


                </div>




            </Fragment>
        )
    }
}


const mapStateToProps = (state) => ({
    books: state.books
})



export default connect(mapStateToProps)(Books);