import React, { Component } from 'react'
import TableComp from './Table'







class BookTable extends Component {



    renderTableComp = () => {


        if (this.props.books) {
            return <TableComp
                tableheads={['#', 'name', 'author', 'articles']}
                rows={this.props.books.books}
            />
        } else {
            return <TableComp
                tableheads={['#', 'name', 'author', 'articles']}
                rows={[]}
            />
        }

    }


    render() {









        return (
            <div className="container">

                {this.renderTableComp()}


            </div>
        )
    }
}








export default BookTable