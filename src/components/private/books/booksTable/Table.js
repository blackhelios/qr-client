import React from 'react'
import { Table } from 'reactstrap'

import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { getBook, deleteBook } from '../../../../actions/bookActions'


function TableComp({

    tableheads,
    rows,
    history,
    getBook,
    deleteBook

}) {

    const editBookHandler = (book) => {

        getBook(book, history)
    }
    const deleteBookHandler = (bookId) => {
        deleteBook(bookId)
    }


    const renderTableHeads = () => {

        return tableheads.map((head) => {

            return (

                <th>{head}</th>


            )

        })

    }


    const renderTableRows = () => {

        if (rows) {

            console.log(rows);

            return rows.map((row, i) => {

                return (
                    <tr>
                        <th scope="row">{i + 1}</th>
                        <td>{row.name}</td>
                        <td>{row.author}</td>
                        <td>{row.articles.length}</td>
                        <td>
                            <button
                                className="btn btn-success btn-sm"
                                onClick={
                                    () => {
                                        editBookHandler(row)
                                    }

                                }

                            >
                                edit
                        </button>
                        </td>
                        <td>
                            <button
                                className="btn btn-warning btn-sm"
                                onClick={
                                    () => {
                                        deleteBookHandler(row._id)
                                    }

                                }

                            >
                                delete
                        </button></td>
                    </tr>
                )
            })


        }



    }

    const renderTable = () => {


        return (

            <Table>
                <thead>
                    <tr>
                        {renderTableHeads()}
                    </tr>
                </thead>
                <tbody>

                    {renderTableRows()}

                </tbody>

            </Table>

        )


    }




    return (

        <div>

            {renderTable()}


        </div>


    )
}




const mapDispatchToProps = {
    getBook,
    deleteBook
}




export default connect(null, mapDispatchToProps)(withRouter(TableComp));