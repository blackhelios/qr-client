import React, { Component, Fragment } from 'react'
import SubHeader from '../../styles/SubHeaderBar/SubHeaderBar'
import AddArticles from './ArticleCRUD/AddArticle/AddArticle'
import ArticlesHome from './ArticlesHome/ArticlesHome'
import { Switch, Route } from 'react-router-dom'

import { connect } from 'react-redux'
import AddArticle from './ArticleCRUD/AddArticle/AddArticle';



class Articles extends Component {

    state = {

        bookOptions: [],

    }


    renderBookOptions = () => {


        let options;
        const booksFrom = this.props.books

        if (booksFrom) {

            options = booksFrom.map((book) => {

                return { key: book.name, value: book._id }
            })


            this.setState({
                bookOptions: options
            })

        }





    }


    componentDidMount = () => {

        this.renderBookOptions()




    }




    render() {


        return (
            <Fragment>
                <SubHeader

                    title="Manage Articles"
                    linkTo="/admin/articles"
                    color="green"
                    link1="/admin/articles/add"
                    link2="/admin/articles"
                    buttontTxt="add articles"

                />
                <div className="container">

                    <div className="row p-2">

                        <div className="col-12">

                            <Switch>
                                <Route exact path="/admin/articles" component={() => <ArticlesHome newArticle={this.state.newArticle} />}

                                />
                                <Route path="/admin/articles/add" component={() => <AddArticle bookOptions={this.state.bookOptions} />} />

                            </Switch>







                        </div>


                    </div>


                </div>
            </Fragment>
        )
    }
}

const mapStateToProps = (state) => ({
    books: state.books.books,

})

const mapDispatchToProps = {

}


export default connect(mapStateToProps, mapDispatchToProps)(Articles);