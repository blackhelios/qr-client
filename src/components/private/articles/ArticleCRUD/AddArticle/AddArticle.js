import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import Form from './Form/Form'
import draftToHtml from 'draftjs-to-html';
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import htmlToDraft from 'html-to-draftjs';
import { addArticle, editArticle } from '../../../../../actions/articlesActions'


class AddArticle extends Component {


    constructor(props) {
        super(props);
        const html = this.props.initialValues ? this.props.initialValues.body : '';
        const contentBlock = htmlToDraft(html);
        if (contentBlock) {
            const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
            const editorState = EditorState.createWithContent(contentState);
            this.state = {
                editorState
            };
        }
    }


    onEditorStateChange = (editorState) => {
        this.setState({
            editorState
        });
    };




    submitHandler = (data) => {


        if (!this.props.initialValues) {
            let bodyData = draftToHtml(convertToRaw(this.state.editorState.getCurrentContent()));
            let dataToSubmit = {
                title: data.title,
                subtitle: data.subtitle,
                body: bodyData,
                images: this.props.images ? this.props.images : [],
                articleLink: {
                    title: data.titleLink,
                    subtitle: data.subtitleLink
                },
                book: data.book
            }


            this.props.addArticle(dataToSubmit)
        } else {
            let bodyData = draftToHtml(convertToRaw(this.state.editorState.getCurrentContent()));
            let dataToSubmit = {
                title: data.title,
                subtitle: data.subtitle,
                body: bodyData,
                images: this.props.images ? this.props.images : [],
                articleLink: {
                    title: data.titleLink,
                    subtitle: data.subtitleLink
                },
                book: data.book
            }

            console.log(dataToSubmit);
            this.props.editArticle(this.props.initialValues._id, dataToSubmit)


        }



    }






    render() {






        return (
            <Fragment>
                <div className="container">

                    <h4 className="display-4 text-center">Add Article</h4>
                    <Form
                        submitCB={this.submitHandler}
                        initialValues={this.props.initialValues}
                        bookOptions={this.props.bookOptions}
                        onEditorStateChange={this.onEditorStateChange}
                        editorState={this.state.editorState}
                    />

                </div>

            </Fragment>
        )
    }
}



const mapStateToProps = (state) => ({
    initialValues: state.articles.articleToEdit,
    images: state.articles.uploadedImages
})

const mapDispatchToProps = {
    addArticle,
    editArticle
}


export default connect(mapStateToProps, mapDispatchToProps)(AddArticle);