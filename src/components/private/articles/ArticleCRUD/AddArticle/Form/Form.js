import React from 'react'
import { reduxForm, Field } from 'redux-form'
import TextInput from '../../../../../misc/forms/inputs/TextInput'
import SelectInput from '../../../../../misc/forms/inputs/SelectInput'
import ImageUpload from '../../../../../misc/forms/files/FileUpload'
import { Editor } from 'react-draft-wysiwyg'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';


import styles from './Form.module.css'


function Form({

    submitCB,
    submitting,
    pristine,
    handleSubmit,
    errors,
    bookOptions,
    initialValues,
    onEditorStateChange,
    editorState
}) {





    return (
        <form
            onSubmit={handleSubmit(submitCB)}

        >
            <label>Select Book</label>
            <Field
                component={SelectInput}
                name="book"
                options={bookOptions}
                multiple={false}
                placeholder='select book'


            />

            <Field
                component={TextInput}
                name="title"
                type="number"
                placeholder="add title"

            />

            <Field
                component={TextInput}
                name="subtitle"
                type="number"
                placeholder="add subtitle"

            />

            <h5 className="text-center mt-2">Write Here</h5>

            <Editor
                wrapperClassName={styles.editorWrapper}
                editorClassName={styles.editor}
                editorState={editorState}
                onEditorStateChange={onEditorStateChange}


            />


            <ImageUpload

                imagesToEdit={initialValues ? initialValues : null}

            />

            <h5 className="text-center mt-2 bg-green my-2">Upload audio file [coming soon... ]</h5>

            <h4 className="text-center mt-3">Other Article to link</h4>
            <div className="container">
                <Field
                    component={TextInput}
                    name="titleLink"
                    type="number"
                    placeholder="title for link article"

                />

                <Field
                    component={TextInput}
                    name="subtitleLink"
                    type="number"
                    placeholder="subtitle for link article"

                />

            </div>


            <button
                className="btn btn-success btn-block "
                type="submit"
            >

                {initialValues ? 'edit article' : 'add article'}

            </button>



        </form>
    )
}




export default reduxForm(

    {
        form: 'addarticle'
    }

)(Form);