
import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { Link, withRouter } from 'react-router-dom'
import { getArticle, deleteArticle } from '../../../../actions/articlesActions'


class ArticleDisplay extends Component {








    renderQueryArticle = () => {

        if (this.props.searchedArticle && !this.props.errors.error) {



            return (


                <Fragment>
                    <div className="container" className="container p-3" style={{
                        backgroundColor: '#fff'

                    }}>

                        <div className="row">
                            <div className="col-lg-6 col-md-6 col-sm-12">
                                <div className="text-center my-2 py-2">
                                    <Link
                                        to=""
                                        style={{
                                            textDecoration: 'none',



                                        }}
                                    >
                                        Go to  title {this.props.searchedArticle ? this.props.searchedArticle.title : 'title'} subtitle{this.props.searchedArticle ? this.props.searchedArticle.articleLink.subtitle : 'subtitle'}


                                    </Link>

                                    {this.props.user.isAuth &&
                                        (<div>
                                            <button
                                                className="btn btn-primary btn-sm"
                                                onClick={
                                                    () => {



                                                        this.props.getArticle(this.props.searchedArticle, this.props.history)
                                                    }
                                                }
                                            >edit</button>
                                            <button
                                                className="btn btn-warning btn-sm"
                                                onClick={
                                                    () => {



                                                        this.props.deleteArticle(this.props.searchedArticle._id)
                                                    }
                                                }
                                            >delete</button>

                                        </div>)




                                    }
                                </div>

                            </div>
                            <div className="col-lg-6 col-md-6 col-sm-12">

                                <img src={this.props.searchedArticle.images[0].url}
                                    className="img-thumbnail"
                                    style={{
                                        borderRadius: '50%',
                                        width: '200px',
                                        height: '200px'
                                    }}
                                />

                            </div>
                            <div


                                dangerouslySetInnerHTML={{ __html: this.props.searchedArticle.body }}

                            >


                            </div>

                        </div>

                    </div>






                </Fragment>



            )

        }







    }



    render() {



        console.log(this.props.searchedArticle)


        return (
            <div className="container" >
                {this.renderQueryArticle()}
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    searchedArticle: state.articles.searchedArticle,
    user: state.user.userData,
    errors: state.errors
})

const mapDispatchToProps = {
    getArticle,
    deleteArticle
}


export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ArticleDisplay));