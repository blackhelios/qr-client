
import React from 'react'
import { reduxForm, Field } from 'redux-form'
import SelectInput from '../../../../misc/forms/inputs/SelectInput'
import TextInput from '../../../../misc/forms/inputs/TextInput'



function Form({

    submitCB,
    submitting,
    pristine,
    errors,
    initialValues,
    handleSubmit,
    bookOptions


}) {


    return (
        <form
            onSubmit={handleSubmit(submitCB)}
        >
            <div className="container text-center">

                <div className="row">

                    <div className="col-lg-4">
                        <Field
                            component={SelectInput}
                            options={bookOptions}
                            name="book"
                            multiple={false}
                            placeholder='select book'


                        />
                    </div>
                    <div className="col-lg-4">
                        <Field
                            component={TextInput}
                            type="number"
                            name="title"
                            placeholder="title"

                        />
                    </div>
                    <div className="col-lg-4">
                        <Field
                            component={TextInput}
                            type="number"
                            name="subtitle"
                            placeholder="subtitle"

                        />
                    </div>

                </div>

                <button
                    type="submit"
                    className="btn btn-md btn-success  pt-2 mt-2"

                >
                    search article
                        </button>

            </div>






        </form>
    )
}


export default reduxForm({

    form: 'searcharticle'

})(Form);
