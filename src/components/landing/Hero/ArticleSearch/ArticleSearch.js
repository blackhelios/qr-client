import React, { Component } from 'react'
import styles from './ArticleSearch.module.css'
import Form from './Form/Form'
import { connect } from 'react-redux'
import { searchArticle } from '../../../../actions/articlesActions'



class ArticleSearch extends Component {

    state = {
        bookOptions: []
    }

    handleSubmitHandler = (data) => {

        this.props.searchArticle(data)


    }


    renderBookOptions = () => {


        let options;
        const booksFrom = this.props.books

        if (booksFrom) {

            options = booksFrom.map((book) => {

                return { key: book.name, value: book._id }
            })



            this.setState({
                bookOptions: options
            })
        }

    }


    componentDidMount = () => {

        this.renderBookOptions()




    }





    render() {



        const renderForm = () => {
            if (this.state.bookOptions) {
                return (
                    <Form
                        submitCB={this.handleSubmitHandler}
                        bookOptions={this.state.bookOptions}

                    />
                )
            }
        }




        return (
            <div className={`container ${styles.searchWrapper} my-4 py-3`}>



                {renderForm()}




            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    books: state.books.books
})



const mapDispatchToProps = {
    searchArticle
}


export default connect(mapStateToProps, mapDispatchToProps)(ArticleSearch)