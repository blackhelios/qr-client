import React, { Component } from 'react'
import styles from './Hero.module.css'
import { connect } from 'react-redux'

import { Link, RichText, Date } from 'prismic-reactjs';
import Prismic from 'prismic-javascript';
import PrismicHelper from '../../../services/cms/Prismic'

import LoadingComponent from '../../misc/Loading/LoadingComponent'

import ArticleSearch from './ArticleSearch/ArticleSearch'
import ArticleDisplay from './ArticleDisplay/ArticleDisplay'



class Hero extends Component {


    state = {
        doc: null
    }



    componentDidMount = () => {

        const apiEndPoint = `https://quren-reader.cdn.prismic.io/api/v2`;

        Prismic.api(apiEndPoint)
            .then((api) => {


                api.query(
                    Prismic.Predicates.at('document.type', 'header'),
                    { lang: '*' }

                )
                    .then((response) => {


                        if (response) {
                            const dataBack = {
                                english: response.results[0].data,
                                myanmar: response.results[1].data
                            }

                            this.setState({
                                doc: dataBack
                            })

                        }







                    })





            })









    }



    renderHero = () => {

        if (this.state.doc) {

            switch (this.props.locale.lang) {
                case 'english':
                    return (
                        <div
                            className={`${styles.heroWrapper}`}
                            style={{ overflow: 'auto' }}
                        >


                            <h4 className={`${styles.headerText}`}>
                                {this.state.doc.english.headertext[0].text}

                            </h4>
                            <p className={`${styles.headerParagraph}`}>
                                {this.state.doc.english.header_paragraph[0].text}
                            </p>






                            <ArticleSearch


                            />

                            <div
                                className="my-3"
                            >

                                <ArticleDisplay />

                            </div>




                        </div>
                    )
                    break;
                case 'myanmar':
                    return (
                        <div
                            className={`${styles.heroWrapper}`}
                            style={{ height: window.innerHeight }}
                        >


                            <h4 className={`${styles.headerText}`}>
                                {this.state.doc.myanmar.headertext[0].text}

                            </h4>
                            <p className={`${styles.headerParagraph}`}>
                                {this.state.doc.myanmar.header_paragraph[0].text}
                            </p>
                            <ArticleSearch

                            />

                            <div
                                className="my-3"
                            >

                                <ArticleDisplay />

                            </div>


                        </div>
                    )
                    break;


                default:
                    break;
            }

        } else {

            return (
                <LoadingComponent />
            )


        }



    }

    render() {

        return (
            <div>

                {this.renderHero()}



            </div>

        )
    }
}


const mapStateToProps = (state) => ({
    locale: state.locale
})

const mapDispatchToProps = {

}


export default connect(mapStateToProps)(Hero);