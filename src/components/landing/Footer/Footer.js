import React from 'react'

import styles from './Footer.module.css'


function Footer() {





    return (



        <div className={`${styles.footerClean} bg-green`}>
            <footer>

                <div className="container">

                    <div className="row justify-content-center">
                        <div className={``}>

                            <h3>Contributed by Ko Kyaw Thura</h3>

                        </div>
                    </div>

                    <div className="row justify-content-center">
                        <div >
                            <a className={`${styles.item}`} href="#"><i className="fa fa-facebook"></i></a>
                            <a className={`${styles.item}`} href="#"><i className="fa fa-twitter"></i></a>
                            <a className={`${styles.item}`} href="#"><i className="fa fa-youtube"></i></a>


                        </div>
                    </div>
                    {/* <div className="row justify-content-center">
                        <p className={`${styles.copyright}`}> Quren Reader © 2019</p>
                    </div> */}
                    <div className="row justify-content-center">
                        <p className={`${styles.copyright}`}> Developed by <a className={`${styles.companyLink}`} href="https://www.facebook.com/blackheliosmm/">Black Helios</a></p>
                    </div>



                </div>



            </footer>
        </div>
    )
}



export default Footer;