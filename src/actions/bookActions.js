
import { GET_BOOKS, ADD_BOOK, EDIT_BOOK, GET_BOOK, DELETE_BOOK } from './types'
import axios from 'axios'
import axiosService from '../services/axiosService'
import { asyncActionStart, asyncActionFinish, asyncActionError } from './asyncActions'
import { endpoint, prodEndpoint } from '../config'
import { toastr } from 'react-redux-toastr'
import { reset } from 'redux-form'

const URL = process.env.NODE_ENV === 'development' ? endpoint : prodEndpoint
const axiosInstance = axiosService.getInstance()




// adding book 
export const addBook = (newBook) => {



    return (dispatch) => {

        // axios.post(`${URL}api/v1/books/add`, newBook)
        axiosInstance.post('/books/add', newBook)
            .then((response) => {

                dispatch({
                    type: ADD_BOOK,
                    payload: response.data.book
                })


                toastr.success('Book Added', 'new book added!')
                dispatch(reset('addbook'))


            })
            .catch((error) => {

                console.log(error)

            })




    }
}


export const getAllBooks = () => {


    return (dispatch) => {
        dispatch(asyncActionStart())
        axiosInstance.get('/books/all')
            .then((response) => {

                dispatch({
                    type: GET_BOOKS,
                    payload: response.data.books
                })

                dispatch(asyncActionFinish())

            })

            .catch((err) => {
                console.log(err)
            })


    }


}


export const editBook = (bookId, data) => {

    return (dispatch) => {

        dispatch(asyncActionStart())
        axiosInstance.post(`/books/edit/${bookId}`, data)
            .then((response) => {

                dispatch({
                    type: EDIT_BOOK,
                    payload: response.data.book
                })

                toastr.success('Book edited', 'book is edited')

                dispatch(asyncActionFinish())



            })

            .catch((err) => {
                console.log(err)
            })

    }


}

export const getBook = (book, history) => {

    return (dispatch) => {
        dispatch({
            type: GET_BOOK,
            payload: book
        })

        history.push(`/admin/books/add`)
    }

}

export const deleteBook = (bookId, history) => {

    return (dispatch) => {
        dispatch(asyncActionStart())
        axiosInstance.post(`/books/delete/${bookId}`)
            .then((response) => {


                dispatch({
                    type: DELETE_BOOK

                })
                dispatch(asyncActionFinish())
                window.location.reload()

            })

            .catch((err) => {
                console.log(err)
            })


    }

}