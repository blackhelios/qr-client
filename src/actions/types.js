

// Users
export const LOGIN_USER = 'login_user';
export const REGISTER_USER = 'register_user';
export const LOGOUT_USER = 'logout_user';
export const AUTH_USER = 'auth_user';


// Async
export const ASYNC_ACTION_START = "ASYNC_ACTION_START";
export const ASYNC_ACTION_FINISH = "ASYNC_ACTION_FINISH"
export const ASYNC_ACTION_ERROR = "ASYNC_ACTION_ERROR"

// locale CMS
export const LOCALE_SET = 'LOCALE_SET';

// Errors
export const GET_ERRORS = 'GET_ERRORS'
export const CLEAR_ERRORS = 'CLEAR_ERRORS';

// Books

export const GET_BOOKS = 'GET_BOOKS';
export const GET_BOOK = 'GET_BOOK';
export const ADD_BOOK = 'ADD_BOOK';
export const EDIT_BOOK = 'EDIT_BOOK';
export const DELETE_BOOK = 'DELETE_BOOK';



// Articles


export const GET_ARTICLE = 'GET_ARTICLE';
export const ADD_ARTICLE = 'ADD_ARTICLE';
export const EDIT_ARTICLE = 'EDIT_ARTICLE';
export const DELETE_ARTICLE = 'DELETE_ARTICLE';
export const SEARCH_ARTICLE = 'SELECT_ARTICLE';

export const UPLOAD_IMAGES = 'UPLOAD_IMAGES'
export const UPLOAD_ARTICLES = 'UPLOAD_ARTICLES'
export const DELETE_IMAGE = 'DELETE_IMAGE'
export const CLEAR_IMAGES = 'CLEAR_IMAGES'