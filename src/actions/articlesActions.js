import { ADD_ARTICLE, GET_ARTICLE, EDIT_ARTICLE, DELETE_ARTICLE, DELETE_IMAGE, CLEAR_IMAGES, UPLOAD_ARTICLES, SEARCH_ARTICLE, GET_ERRORS, CLEAR_ERRORS, UPLOAD_IMAGES } from './types'
import { endpoint, prodEndpoint } from '../config'
import axios from 'axios'
import axiosService from '../services/axiosService'
import { reset } from 'redux-form'
import { asyncActionStart, asyncActionFinish, asyncActionError } from './asyncActions'
import { toastr } from 'react-redux-toastr'
const axiosInstace = axiosService.getInstance()


const URL = process.env.NODE_ENV === 'development' ? endpoint : prodEndpoint

export const addArticle = (dataToSubmit) => {

    return (dispatch) => {
        dispatch(asyncActionStart())
        axiosInstace.post(`/articles/add`, dataToSubmit)
            .then((response) => {

                console.log(response.data.success)

                if (response.data.success === false) {
                    dispatch({
                        type: GET_ERRORS,
                        payload: response.data.message
                    })
                    dispatch(asyncActionFinish())

                }
                dispatch({
                    type: ADD_ARTICLE,
                    payload: response.data.article


                })

                dispatch({
                    type: CLEAR_IMAGES
                })
                dispatch(asyncActionFinish())

                dispatch({
                    type: CLEAR_IMAGES
                })

                toastr.success('Article', 'add article succeess')




            })

            .catch((err) => {
                console.log(err);
            })




    }



}
export const editArticle = (articleId, dataToSubmit) => {

    return (dispatch) => {

        axiosInstace.post(`/articles/edit/${articleId}`, dataToSubmit)
            .then((response) => {
                dispatch(asyncActionStart())
                console.log(response.data.success)

                if (response.data.success === false) {
                    dispatch({
                        type: GET_ERRORS,
                        payload: response.data.message
                    })
                    dispatch(asyncActionFinish())
                }
                dispatch({
                    type: EDIT_ARTICLE,
                    payload: response.data.article


                })
                dispatch(asyncActionFinish())
                toastr.success('Article', 'edit article succeess')




            })

            .catch((err) => {
                console.log(err);
            })




    }



}


export const addToImagesArr = (imageUrl) => {


    return {
        type: UPLOAD_IMAGES,
        payload: imageUrl
    }


}


export const sendImage = (image) => {


    return (dispatch) => {

        const formData = new FormData()
        formData.append('file', image)

        const config = {
            header: { 'content-type': 'multipart/form-data' }
        }
        toastr.success('image uploading', 'wait a sec')
        axiosInstace.post(`/articles/image`, formData, config)
            .then((response) => {
                console.log(response.data.url);
                dispatch(addToImagesArr(response.data))



            })
            .catch((err) => {
                console.log(err)
            })


    }






}

export const getArticle = (book, history) => {

    return (dispatch) => {
        dispatch({
            type: GET_ARTICLE,
            payload: book
        })

        history.push(`/admin/articles/add`)
    }

}

export const searchArticle = (dataToSubmit) => {


    return (dispatch) => {


        axiosInstace.post(`/articles/search`, dataToSubmit)

            .then((response) => {

                if (response.data.success) {


                    dispatch({
                        type: SEARCH_ARTICLE,
                        payload: response.data.article

                    })
                    dispatch({
                        type: CLEAR_ERRORS,

                    })
                    dispatch(reset('searcharticle'))

                } else {
                    dispatch({
                        type: GET_ERRORS,
                        payload: response.data.message
                    })
                    toastr.warning('Article Not Found', 'we are re-editing all articles .. currently not search available')
                    dispatch(reset('searcharticle'))


                }




                // setTimeout(() => {
                //     window.location.reloa
                // }, 500);





            })

            .catch((err) => {
                console.log(err)
            })




    }

}

export const deleteArticle = (articleId) => {

    return (dispatch) => {
        dispatch(asyncActionStart())
        axiosInstace.post(`/articles/delete/${articleId}`)
            .then((response) => {


                dispatch({
                    type: DELETE_ARTICLE

                })
                dispatch(asyncActionFinish())
                window.location.reload()

            })

            .catch((err) => {
                console.log(err)
            })


    }

}



export const deleteImage = (imageId) => {



    return (dispatch) => {

        axiosInstace.post(`/articles/image/${imageId}`)
            .then((response) => {


                dispatch({
                    type: DELETE_IMAGE,
                    payload: imageId
                })

                toastr.success('Image Delete', 'delete image successfully')

            })

            .catch((err) => {
                console.log(err);
            })



    }


}